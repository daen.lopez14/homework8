package com.example.domain.interaction

import com.example.domain.model.Photos
import com.example.domain.repository.PhotoRepository
import io.reactivex.rxjava3.core.Flowable

class GetAllPhotosUseCaseImpl(private val photoRepository: PhotoRepository) : GetAllPhotosUseCase {
    override fun invoke(): Flowable<List<Photos>> = photoRepository.getAllPhotos()
}