package com.example.domain.interaction

import io.reactivex.rxjava3.core.Completable

interface RefreshPhotosUseCase {
    suspend operator fun invoke(): Unit
}