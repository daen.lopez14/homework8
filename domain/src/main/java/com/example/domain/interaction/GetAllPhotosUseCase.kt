package com.example.domain.interaction

import com.example.domain.model.Photos
import io.reactivex.rxjava3.core.Flowable

interface GetAllPhotosUseCase {
    operator fun invoke(): Flowable<List<Photos>>
}