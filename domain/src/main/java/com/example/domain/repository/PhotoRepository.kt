package com.example.domain.repository

import androidx.lifecycle.LiveData
import com.example.domain.model.Photos
import io.reactivex.rxjava3.core.Flowable


interface PhotoRepository {

    fun getAllPhotos(): Flowable<List<Photos>>

    suspend fun refreshPhotos(): Unit

}