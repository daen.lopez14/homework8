package com.example.data

import com.example.data.networking.PhotosApi
import com.example.data.networking.model.PhotoInfoResponse

class FakeRemoteDataSource : PhotosApi{
    override suspend fun getPhotos(start: Int, limit: Int): List<PhotoInfoResponse> {
        return listOf<PhotoInfoResponse>()
    }
}