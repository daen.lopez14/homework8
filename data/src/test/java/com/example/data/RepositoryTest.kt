package com.example.data

import com.example.data.repository.PhotoRepositoryImpl
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class RepositoryTest {

    private lateinit var remoteDataSource: FakeRemoteDataSource
    private lateinit var localDataSource: FakePhotoDao

    // Class under test
    private lateinit var defaultRepository: PhotoRepositoryImpl

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule1()

    @Before
    fun createRepository() {
        remoteDataSource = FakeRemoteDataSource()
        localDataSource = FakePhotoDao()
        // Get a reference to the class under test
        // Given a class
        defaultRepository = PhotoRepositoryImpl(remoteDataSource, localDataSource)
    }

    @Test
    fun getTasks_requestsAllTasksFromRemoteDataSource() = mainCoroutineRule.runBlockingTest {
        // When tasks are requested from the tasks repository
        val photos = defaultRepository.getAllPhotos()

        // Then tasks are loaded from the remote data source
        MatcherAssert.assertThat(photos, (CoreMatchers.not(CoreMatchers.nullValue())))
    }
}