package com.example.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.data.database.dao.PhotosDao
import com.example.data.database.model.PhotosEntity


class FakePhotoDao: PhotosDao {
    override fun getPhotos(): LiveData<List<PhotosEntity>> {
        return MutableLiveData<List<PhotosEntity>>()
    }

    override fun insertAll(photos: List<PhotosEntity>) {
        TODO("Not yet implemented")
    }
}