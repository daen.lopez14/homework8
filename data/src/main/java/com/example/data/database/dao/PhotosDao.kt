package com.example.data.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.database.model.PhotosEntity
import io.reactivex.rxjava3.core.Flowable

@Dao
interface PhotosDao {

    //se obtiene una lista de objetos con modelado photosEntity.
    @Query("SELECT * FROM photosEntity")
    fun getPhotos(): Flowable<List<PhotosEntity>>

    //La base de datos guarda objetos de tipo PhotosEntity. una lista de estos objetos
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(photos: List<PhotosEntity>)


}