package com.example.homework5beta1.ui.itemsList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.Photos
import com.example.homework5beta1.databinding.ItemsPostBinding

class ItemsPostAdapter : ListAdapter<Photos,
        ItemsPostAdapter.PostsViewHolder>(DiffCallback) {

    var _navigateToSelectedPhoto1 = MutableLiveData<Photos?>()

    class PostsViewHolder(private var binding: ItemsPostBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(post: Photos) {
            binding.photo = post
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        return PostsViewHolder(
            ItemsPostBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.itemView.setOnClickListener {
            _navigateToSelectedPhoto1.value = currentItem
        }
        holder.bind(currentItem)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Photos>() {
        override fun areItemsTheSame(oldItem: Photos, newItem: Photos): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Photos, newItem: Photos): Boolean {
            return oldItem.title == newItem.title
        }
    }
}