package com.example.homework5beta1.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interaction.GetAllPhotosUseCase
import com.example.domain.interaction.RefreshPhotosUseCase
import com.example.domain.model.Photos
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ItemListViewModel @Inject constructor(
    val getAllPhotosUseCase: GetAllPhotosUseCase,
    val refreshPhotosUseCase: RefreshPhotosUseCase
) : ViewModel() {

    val disposable = CompositeDisposable()

    var _navigateToSelectedPhoto = MutableLiveData<Photos?>()
    val navigateToSelectedPhoto: LiveData<Photos?> = _navigateToSelectedPhoto


    init {
        refreshData()
    }

    private fun refreshData() {
        viewModelScope.launch {
            refreshPhotosUseCase.invoke()
        }
    }

    fun returnPhotos(): Flowable<List<Photos>> {
        return getAllPhotosUseCase.invoke().map { list: List<Photos> ->
            list
        }
    }

    fun displayPhotoDetails(photo: Photos) {
        _navigateToSelectedPhoto.value = photo
    }

    fun displayPhotoDetailsComplete() {
        _navigateToSelectedPhoto.value = null
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}