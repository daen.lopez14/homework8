package com.example.homework5beta1.utils

import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.domain.model.Photos
import com.example.homework5beta1.R
import com.example.homework5beta1.ui.itemsList.ItemsPostAdapter
import com.example.homework5beta1.viewmodel.ItemListViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers

@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: ItemListViewModel) {
    val observer = data.returnPhotos()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeBy(
            onNext = {
                val adapter = recyclerView.adapter as ItemsPostAdapter
                adapter.submitList(it)
            }, onError = {Log.d("Error", "FAILURE LOADING DATA")}
        )
    data.disposable.add(observer)
}

@BindingAdapter("imageUrl")
fun bindImage3(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        imgView.load(imgUrl) {
            placeholder(R.drawable.loading_animation)
            error(R.drawable.ic_broken_image)
        }
    }
}
